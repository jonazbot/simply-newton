# Simply Newton
A simple wrapper for https://github.com/aunyks/newton-api

## URL
```url
https://www.simplynewton.infinityfreeapp.com/ 
```

## Use
Enter semi-colon seperated expressions into the text-area and press the button that corresponds to the desired function.

**Example:**
```
4x*5x;y^3*3y;8t/2t;1+1
```
***or***
```
4x*5x;
y^3*3y;
8t/2t;
1+1
```
